package com.example.bloodproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class managepost_show_ extends AppCompatActivity {
    LinearLayout linearLayout;
    ListView listView;
    DatabaseReference databaseReference,coutraparenc,dra;
    public List<ShoutHistory> inlist;
    public custom_adptar_postall ff;
    FirebaseAuth firebaseAuth,auth;
    FirebaseUser user;
    ImageView delete;
    private String currntuserId;
    private ProgressDialog progressBar;
    FirebaseDatabase firebaseDatabase;
    static  int val=0;
    ShoutHistory insdfert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_managepost_show_);

        delete=(ImageView)findViewById(R.id.deletelist);
        listView=(ListView)findViewById(R.id.allpostshow);
        linearLayout=(LinearLayout)findViewById(R.id.linarlayout);
        databaseReference= FirebaseDatabase.getInstance().getReference("ShoutHistory");
        inlist =new ArrayList<>();
        ff  =new custom_adptar_postall(managepost_show_.this,inlist);

        progressBar = new ProgressDialog(this);
        auth=FirebaseAuth.getInstance();
        currntuserId=auth.getCurrentUser().getUid();
        coutraparenc=FirebaseDatabase.getInstance().getReference().child("ShoutHistory");

    }
    @Override
    protected void onStart() {


        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("ShoutHistory");
        Query query=databaseReference.orderByChild("managed_or_unmanaged").equalTo("Managed");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    ShoutHistory inserty = ds.getValue(ShoutHistory.class);
                    inlist.add(inserty);


                }
                Collections.reverse((List<?>) inlist);
                listView.setAdapter(ff);
                progressBar.dismiss();

                val = listView.getAdapter().getCount();


                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        for (int i = 0; i < listView.getCount(); i++)


                            if (position==i){
                                insdfert=inlist.get(position);
                                //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl(),Toast.LENGTH_LONG).show();
                            }
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplication(), "error" + databaseError.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        super.onStart();

    }
    public void backhomepage(View view) {
        startActivity(new Intent(managepost_show_.this,cardview_home.class));
        customType(managepost_show_.this,"bottom-to-up");

        finish();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(managepost_show_.this,cardview_home.class));
            customType(managepost_show_.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void callnumbers(View view)  {



        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {
            callnumber();

        }


    }
    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }


    public void callnumber(){
        final String phonenumber=insdfert.getPhone();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("CAll");
        alertDialogBuilder.setIcon(R.drawable.ic_call_black_24dp);
        alertDialogBuilder.setMessage("Phone Number:"+phonenumber+"");


        alertDialogBuilder.setPositiveButton("Call Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        dialContactPhone(""+phonenumber+"");



                    }
                });

        alertDialogBuilder.setNegativeButton("No Call",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }





}