package com.example.bloodproject;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



import com.google.android.gms.common.internal.service.Common;
import com.google.firebase.auth.FirebaseAuth;
import com.nex3z.notificationbadge.NotificationBadge;
import com.smarteist.autoimageslider.DefaultSliderView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import static maes.tech.intentanim.CustomIntent.customType;

public class cardview_home extends AppCompatActivity {
    LocationManager locationManager;
    TextView marqueText;
    SharedPreferences sp;
    SliderLayout sliderLayout;

    TextView smsCountTxt;

    int pendingSMSCount;


    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardview_home);

        sliderLayout = findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(IndicatorAnimations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(1);//set scroll delay in seconds :
        sp = getSharedPreferences("login",MODE_PRIVATE);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref",  Context.MODE_PRIVATE);
        pendingSMSCount=+pref.getInt("valu",0);

        //allpost_show shoutHistoryShow=new allpost_show();
        //pendingSMSCount=shoutHistoryShow.valushare;


        setSliderViews();

        Toolbar toolbar = (Toolbar) findViewById(R.id.hj);
        toolbar.setTitle("Duty Apps");
        setSupportActionBar(toolbar);

     marqueText=(TextView)findViewById(R.id.markueText);
        marqueText.setSelected(true);










    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.homepage, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_settings);

        View actionView = MenuItemCompat.getActionView(menuItem);
        smsCountTxt = (TextView) actionView.findViewById(R.id.notification_badge);

        setupBadge();
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void setupBadge() {


        if (smsCountTxt != null) {
            if (pendingSMSCount == 0) {
                if (smsCountTxt.getVisibility() != View.GONE) {
                    smsCountTxt.setVisibility(View.GONE);
                }
            } else {
                smsCountTxt.setText(String.valueOf(Math.min(pendingSMSCount, 99)));
                if (smsCountTxt.getVisibility() != View.VISIBLE) {
                    smsCountTxt.setVisibility(View.VISIBLE);
                }
            }}}



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(cardview_home.this,allpost_show.class));
            customType(cardview_home.this,"up-to-bottom");
            finish();
            return true;
        }


        if (id == R.id.logout) {
            alar();
        }

        return super.onOptionsItemSelected(item);
    }
    public void postclick(View view) {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
        else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startActivity(new Intent(cardview_home.this,shoutPage_sendsms.class));

            customType(cardview_home.this,"up-to-bottom");
            finish();


        }
    }

    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    public void yourpostclik(View view) {

        startActivity(new Intent(cardview_home.this,ShoutHistoryShow.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();


        //Intent intent=new Intent(cardview_home.this,ShoutHistoryShow.class);
        //startActivity(intent);

    }


    public void andminuser(View view) {
        Snackbar.make(view, "Coming Soon", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    public void goToMainActivity(){
        startActivity(new Intent(cardview_home.this,login_page2.class));
        customType(cardview_home.this,"up-to-bottom");

        finish();
    }

    public void alar(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure,Log Out Your Account");
                alertDialogBuilder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                FirebaseAuth.getInstance().signOut();
                                goToMainActivity();
                                sp.edit().putBoolean("logged",false).apply();
                                finish();

                                //return true;
                            }
                        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    private void setSliderViews() {

        for (int i = 0; i <= 3; i++) {

            DefaultSliderView sliderView = new DefaultSliderView(this);

            switch (i) {
                case 0:
                    sliderView.setImageDrawable(R.drawable.slide1);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/547114/pexels-photo-547114.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 1:
                    sliderView.setImageDrawable(R.drawable.slide2);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
                case 2:
                    sliderView.setImageDrawable(R.drawable.slide3);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260");
                    break;
                case 3:
                    sliderView.setImageDrawable(R.drawable.slide4);
                    //sliderView.setImageUrl("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260");
                    break;
            }

            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
            sliderView.setDescription("রক্ত দিন জীবন বাঁচান");
            final int finalI = i;
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    Toast.makeText(cardview_home.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
                }
            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }


    public void allpostshow(View view) {
        startActivity(new Intent(cardview_home.this,allpost_show.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();

    }

    public void myaccount(View view) {
        startActivity(new Intent(cardview_home.this,profileUpdate.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();

    }

    public void managepostshow(View view) {
        startActivity(new Intent(cardview_home.this,managepost_show_.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();
    }

    public void aboutpost(View view) {


    }

    public void aboutpage(View view) {
        startActivity(new Intent(cardview_home.this,about_page.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();
    }

    public void fact(View view) {
        startActivity(new Intent(cardview_home.this,donat_blood_14_tips.class));
        customType(cardview_home.this,"up-to-bottom");
        finish();
    }
}
