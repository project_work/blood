package com.example.bloodproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import static maes.tech.intentanim.CustomIntent.customType;

public class ke_kake_donate_blood extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ke_kake_donate_blood);
    }

    public void donatepageback(View view) {
        startActivity(new Intent(ke_kake_donate_blood.this,donat_blood_14_tips.class));
        customType(ke_kake_donate_blood.this,"right-to-left");
        finish();
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(ke_kake_donate_blood.this,donat_blood_14_tips.class));
            customType(ke_kake_donate_blood.this,"right-to-left");
            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void upokareta(View view) {
        startActivity(new Intent(ke_kake_donate_blood.this,blood_day_opokareta.class));
        customType(ke_kake_donate_blood.this,"left-to-right");
        finish();
    }
}
