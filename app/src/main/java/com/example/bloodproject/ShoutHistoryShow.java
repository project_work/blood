package com.example.bloodproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static maes.tech.intentanim.CustomIntent.customType;

public class ShoutHistoryShow extends AppCompatActivity {
    ListView listView;
    LinearLayout linearLayout;
    RadioGroup rg;

    DatabaseReference databaseReference,coutraparenc,dra;
    public List<ShoutHistory> inlist;
    public custom_adptar ff;
    FirebaseAuth firebaseAuth,auth;
    FirebaseUser user;
    ImageView delete;
    private String currntuserId;
    private String managedorunmanaged;
    private ProgressDialog progressBar;
    FirebaseDatabase firebaseDatabase;
    //static  int val=0;
    ShoutHistory insdfert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shout_history_show);
        delete=(ImageView)findViewById(R.id.deletelist);
        listView=(ListView)findViewById(R.id.youpostlist);
        linearLayout=(LinearLayout)findViewById(R.id.linarlayout);
        databaseReference= FirebaseDatabase.getInstance().getReference("ShoutHistory");
        inlist =new ArrayList<>();
        ff  =new custom_adptar(ShoutHistoryShow.this,inlist);

        progressBar = new ProgressDialog(this);

        auth=FirebaseAuth.getInstance();
        progressBar.setTitle("Your Post");
        progressBar.setMessage("Please wait,Loading");
        progressBar.setIcon(R.drawable.posticon);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.show();
        currntuserId=auth.getCurrentUser().getUid();
        coutraparenc=FirebaseDatabase.getInstance().getReference().child("ShoutHistory");

        Showdata();




    }






    public void Showdata(){

        firebaseAuth= FirebaseAuth.getInstance();
        user=firebaseAuth.getCurrentUser();
        firebaseDatabase=FirebaseDatabase.getInstance();
        //databaseReference=firebaseDatabase.getReference("ShoutHistory");
        Query query=databaseReference.orderByChild("email").equalTo(user.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren()){

                    ShoutHistory inserty=ds.getValue(ShoutHistory.class);
                    inlist.add(inserty);



                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            for (int i = 0; i < listView.getCount(); i++)


                                if (position==i){
                                    insdfert=inlist.get(position);
                                    //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl(),Toast.LENGTH_LONG).show();
                                }
                        }
                    });

                }
                Collections.reverse((List<?>) inlist);
                listView.setAdapter(ff);
                progressBar.dismiss();


                //val=listView.getAdapter().getCount();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplication(),"error"+databaseError.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
        super.onStart();
}



    public void backhomepage(View view) {
        startActivity(new Intent(ShoutHistoryShow.this,cardview_home.class));
        customType(ShoutHistoryShow.this,"bottom-to-up");

        finish();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(ShoutHistoryShow.this,cardview_home.class));
            customType(ShoutHistoryShow.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }





    public void deleteshout(View view)  {



        if (insdfert==null){


            Snackbar.make(view, "You Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
         }
        else {


            deletepost();

        }
    }


    public void deletepost(){


        String donate=insdfert.getDonatbl();
        String need=insdfert.getNeedbl();
        String phone=insdfert.getPhone();
        String loaction=insdfert.getLoact();
        String date=insdfert.getDatep();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Delete Post");
        alertDialogBuilder.setIcon(R.drawable.warning);
        alertDialogBuilder.setMessage(" Donate Blood :" +donate+" \n Need Blood :" +need+" \n Phone :"+phone+"\n Location  :"+loaction+"\nDate  :"+date+"");


        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        String sid=insdfert.getSmsUId();

                        //Toast.makeText(getApplication(),"Hello"+insdfert.getNeedbl()+"/n"+insdfert.getPhone()+"",Toast.LENGTH_LONG).show();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        Query applesQuery = ref.child("ShoutHistory").orderByChild("smsUId").equalTo(sid);

                        applesQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                                    appleSnapshot.getRef().removeValue();
                                    inlist.clear();
                                    //Showdata();

                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }
                });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public void callnumbers(View view)  {



        if (insdfert==null){


            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        else {


            callnumber();

        }


    }
    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }


    public void callnumber(){
        final String phonenumber=insdfert.getPhone();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("CAll");
        alertDialogBuilder.setIcon(R.drawable.ic_call_black_24dp);
        alertDialogBuilder.setMessage("Phone Number:"+phonenumber+"");


        alertDialogBuilder.setPositiveButton("Call Now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                            /*Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:999"));
                            if (ActivityCompat.checkSelfPermission(allpost_show.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);*/
                        dialContactPhone(""+phonenumber+"");



                    }
                });

        alertDialogBuilder.setNegativeButton("No Call",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }


    public  void updatemangedorunmaged(View view){
        if (insdfert==null){
            Snackbar.make(view, "Select Post", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();

        }
        else {

            alartdilog();
             }

    }
    public void alartdilog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Post");
        String fff=insdfert.getNeedbl();
        builder.setMessage("Need Blood : " +fff);

        builder.setIcon(R.drawable.ic_update_black_24dp);
        LayoutInflater inflater=getLayoutInflater();
        final View dilogview=inflater.inflate(R.layout.alartddilog_updatedata_input,null);


        builder.setView(dilogview);

        final RadioGroup rg = (RadioGroup)dilogview. findViewById(R.id.radioGroup1);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId) {

                    case R.id.radio0:
                        managedorunmanaged="Managed";

                        break;
                    case R.id.radio1:
                        managedorunmanaged="Unmanaged";
                        break;
                }
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String suid=insdfert.getSmsUId();

                firebaseDatabase = FirebaseDatabase.getInstance();
                databaseReference = firebaseDatabase.getReference();
                databaseReference.child("ShoutHistory").child(suid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        dataSnapshot.getRef().child("managed_or_unmanaged").setValue(managedorunmanaged);
                        progressBar.dismiss();
                        inlist.clear();


                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                       Toast.makeText(getApplication(),"Error"+databaseError,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


}
