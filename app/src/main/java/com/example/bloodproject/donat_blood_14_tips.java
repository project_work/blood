package com.example.bloodproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

import static maes.tech.intentanim.CustomIntent.customType;

public class donat_blood_14_tips extends AppCompatActivity {
    public ListView listView;
    String[] tipsarry;
    public int[] govimage={R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,
            R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donat_blood_14_tips);
        listView=(ListView)findViewById(R.id.tips14);
        tipsarry =getResources().getStringArray(R.array.tipsitam);
        tipslist_adapter cu=new tipslist_adapter(this, govimage,tipsarry);
        listView.setAdapter(cu);

    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(donat_blood_14_tips.this,cardview_home.class));
            customType(donat_blood_14_tips.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void tnj(View view) {
        startActivity(new Intent(donat_blood_14_tips.this,ke_kake_donate_blood.class));
        customType(donat_blood_14_tips.this,"left-to-right");
        finish();
    }

    public void cardpageback(View view) {
        startActivity(new Intent(donat_blood_14_tips.this,cardview_home.class));
        customType(donat_blood_14_tips.this,"right-to-left");
        finish();
    }
}
