package com.example.bloodproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static maes.tech.intentanim.CustomIntent.customType;

public class changepassword_firebase extends AppCompatActivity {
   EditText changed;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    private ProgressDialog progressBar;
    SharedPreferences sp;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword_firebase);
        changed=(EditText)findViewById(R.id.changepassword_box);
        progressBar = new ProgressDialog(this);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        databaseReference= FirebaseDatabase.getInstance().getReference("UserProfile");

    }

    public void backloginLayout(View view) {
        startActivity(new Intent(changepassword_firebase.this,profileUpdate.class));
        customType(changepassword_firebase.this,"up-to-bottom");

        finish();

    }

    public void submitpassword(final View view) {
        final String passwordcha=changed.getText().toString();
        if (passwordcha.isEmpty()){

            changed.setError("Password Null");
            changed.requestFocus();
            return;
        }
        if (passwordcha.length()<6){
            changed.setError("Minimum Password 6 ");
            changed.requestFocus();
            return;

        }
        else{

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Password Change");
        alertDialogBuilder.setIcon(R.drawable.ic_update_black_24dp);
        alertDialogBuilder.setMessage("Your Password:"+passwordcha+"");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                          firebaseAuth= FirebaseAuth.getInstance();
                            user=firebaseAuth.getCurrentUser();
                            if (user!=null){
                                progressBar.setTitle("Change Password");
                                progressBar.setMessage("Your Password:" +changed.getText().toString()+"");
                                progressBar.setIcon(R.drawable.ic_update_black_24dp);
                                progressBar.setCanceledOnTouchOutside(false);
                                progressBar.show();
                                user.updatePassword(passwordcha)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                if (task.isSuccessful()){
                                                    progressBar.dismiss();
                                                    Snackbar.make(view, "Change Password Success ", Snackbar.LENGTH_SHORT)
                                                            .setAction("Action", null).show();
                                                    changepasswordindatabase();
                                                    //firebaseAuth.signOut();
                                                    //Intent intent=new Intent(changepassword_firebase.this,Login_page.class);
                                                    //startActivity(intent);
                                                }

                                                else {
                                                    Snackbar.make(view, "Password Not Change", Snackbar.LENGTH_SHORT)
                                                            .setAction("Action", null).show();
                                                    progressBar.dismiss();
                                                    //Toast.makeText(getApplication(),"Password Not Change",Toast.LENGTH_SHORT).show();

                                                }

                                            }
                                        });}
                        }



                });

        alertDialogBuilder.setNegativeButton("NO",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        }
}

    public void changepasswordindatabase(){
        firebaseAuth= FirebaseAuth.getInstance();
        user=firebaseAuth.getCurrentUser();
        String uid=user.getUid();
        final String password=changed.getText().toString();

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        databaseReference.child("UserProfile").child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().child("Password").setValue(password);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(changepassword_firebase.this,profileUpdate.class));
            customType(changepassword_firebase.this,"up-to-bottom");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
