package com.example.bloodproject;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class custom_adptar_postall extends ArrayAdapter<ShoutHistory> {
    public Activity context;
    public List<ShoutHistory> inlist;


    public custom_adptar_postall(Activity context, List<ShoutHistory> inlist) {
        super(context, R.layout.postall_simple_layout, inlist);
        this.context = context;
        this.inlist = inlist;
    }


    @Override
    public View getView (int position,  View convertView,  ViewGroup parent)   {

        LayoutInflater layoutInflater=context.getLayoutInflater();

        View view=layoutInflater.inflate(R.layout.postall_simple_layout,null,true);
         ShoutHistory insdfert=inlist.get(position);
         TextView needblood=(TextView) view.findViewById(R.id.bloodgrup);
        TextView datea=(TextView) view.findViewById(R.id.datefr);
        TextView name=(TextView) view.findViewById(R.id.names);
        TextView locationa=(TextView) view.findViewById(R.id.locstionfr);
        TextView timea=(TextView) view.findViewById(R.id.times);
        TextView contart=(TextView) view.findViewById(R.id.contactfr);
        TextView donate=(TextView) view.findViewById(R.id.donateblo);


        needblood.setText(""+insdfert.getNeedbl());
        datea.setText(""+insdfert.getDatep());
        name.setText(""+insdfert.getName());
        locationa.setText(""+insdfert.getLoact());

        timea.setText(""+insdfert.getTimep());
        contart.setText(""+insdfert.getPhone());
        donate.setText(""+insdfert.getDonatbl());


        return view;
    }
}
