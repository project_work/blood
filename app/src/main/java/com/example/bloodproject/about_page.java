package com.example.bloodproject;

import android.content.Intent;
import android.icu.util.Calendar;
import android.icu.util.ValueIterator;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

import static maes.tech.intentanim.CustomIntent.customType;

public class about_page extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_about_page);
        Element addelement=new Element();
        View aboutpage=new AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.iconp)
                .setDescription("Universal Trade Center")
                .addItem(Developer())

                .addGroup("Connect with me")
                .addEmail("basudab800@gmail.com")
                .addFacebook("basu0")
                .addTwitter("basu800")
                .addGroup("Connect with UTC CEO")
                .addEmail("shoheluit@gmail.com")
                .addFacebook("shahed.shohel1")
                .addTwitter("basu800")
                .addFacebook("UTCWorldSC")
                . addItem(new Element().setTitle("Version 1.0"))
                .addItem(createCopyright())
                .create();
        setContentView(aboutpage);


    }

    private Element Developer() {
        Element develo=new Element();

        String deve="Designed & Developed By (Basu dev)";
        develo.setTitle(deve);
        develo.setIcon(R.drawable.defultuser);
        develo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return develo;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private Element createCopyright() {
        Element copyright=new Element();
        String copyrit=String.format("Copyright %d by UTC", Calendar.getInstance().get(Calendar.YEAR));
         copyright.setTitle(copyrit);
        copyright.setGravity(Gravity.CENTER);
        copyright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
         Toast.makeText(getApplication(),"NO Description",Toast.LENGTH_SHORT).show();
            }
        });
        return copyright;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(about_page.this,cardview_home.class));
            customType(about_page.this,"up-to-bottom");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
