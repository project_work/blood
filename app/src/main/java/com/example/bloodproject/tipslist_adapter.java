package com.example.bloodproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class tipslist_adapter extends BaseAdapter {

    int[] bloodicon ;
    public String[] bloodtips;
    Context context;
    public LayoutInflater inflater;
    tipslist_adapter (Context context,int[] bloodicon,String[] bloodtips){
        this.context=context;
        this.bloodtips=bloodtips;
        this.bloodicon=bloodicon;


    }


    @Override
    public int getCount() {
        return bloodtips.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.tips14_simple_layout,parent,false);

        }
        ImageView Imageview =(ImageView)convertView.findViewById(R.id.imagre);
        Imageview.setImageResource(bloodicon[position]);
        TextView textView=(TextView)convertView.findViewById(R.id.tips_14text);
        textView.setText(bloodtips[position]);
        return convertView;

    }}