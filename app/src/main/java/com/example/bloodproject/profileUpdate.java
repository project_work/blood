package com.example.bloodproject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import static maes.tech.intentanim.CustomIntent.customType;

public class profileUpdate extends AppCompatActivity {
 EditText emai,name1,blood1,phone,password,confirpassword,address;
    FirebaseAuth firebaseAuth;
    FirebaseUser user;
    private ProgressDialog progressBar;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    SharedPreferences sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_update);
        emai=(EditText)findViewById(R.id.email);
        name1=(EditText)findViewById(R.id.userName);
        blood1=(EditText)findViewById(R.id.bloodedit);
        phone=(EditText)findViewById(R.id.phone);
        password=(EditText)findViewById(R.id.password);
        confirpassword=(EditText)findViewById(R.id.confirampassword);
        address=(EditText)findViewById(R.id.address);
        progressBar = new ProgressDialog(this);
        sp = getSharedPreferences("login",MODE_PRIVATE);

        imagebloodshow();

    }

    public void imagebloodshow(){
        firebaseAuth= FirebaseAuth.getInstance();
        user=firebaseAuth.getCurrentUser();
        firebaseDatabase= FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("UserProfile");
        Query query=databaseReference.orderByChild("email").equalTo(user.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren()){
                    String name=""+ds.child("Name").getValue();
                    String email=""+ds.child("email").getValue();
                    String number=""+ds.child("Phone").getValue();
                    String blood=""+ds.child("Blood").getValue();
                    String image=""+ds.child("image").getValue();
                    String address1=""+ds.child("Address").getValue();
                    String password1=""+ds.child("Password").getValue();
                    emai.setText(email);
                    name1.setText(name);
                    phone.setText(number);
                    blood1.setText(blood);
                    address.setText(address1);
                    password.setText(password1);
                    confirpassword.setText(password1);
                    try {
                        //Picasso.get().load(image).into(imageshow);
                    }
                    catch (Exception e){
                        //Picasso.get().load(R.drawable.basu).into(imageshow);
                        //Toast.makeText(homepage.this,"erroe"+e,Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(profileUpdate.this,cardview_home.class));
            customType(profileUpdate.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void backhomepage(View view) {
        startActivity(new Intent(profileUpdate.this,cardview_home.class));
        customType(profileUpdate.this,"bottom-to-up");

        finish();

    }

    public void changepassword(View view) {
        startActivity(new Intent(profileUpdate.this,changepassword_firebase.class));
        customType(profileUpdate.this,"up-to-bottom");
        finish();


    }

    public void deactiveaccount(final View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Deactive your account");
        alertDialogBuilder.setIcon(R.drawable.deleteicon);
        alertDialogBuilder.setMessage("You Sure Deactive Account");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        FirebaseUser user=FirebaseAuth.getInstance().getCurrentUser();
                        if (user!=null){
                            progressBar.setTitle("Deactive Your Account");
                            progressBar.setMessage("Deactive Your Account,wait please");
                            progressBar.setIcon(R.drawable.deleteicon);
                            progressBar.setCanceledOnTouchOutside(false);
                            progressBar.show();
                            user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){

                                        progressBar.dismiss();
                                        Snackbar.make(view, "Your Account Delete", Snackbar.LENGTH_SHORT)
                                                .setAction("Action", null).show();
                                        FirebaseAuth.getInstance().signOut();
                                        sp.edit().putBoolean("logged",false).apply();
                                        startActivity(new Intent(profileUpdate.this,Login_page.class));
                                        customType(profileUpdate.this,"up-to-bottom");

                                        finish();
                                    }

                                    else {
                                        Snackbar.make(view, "Account not Deactive", Snackbar.LENGTH_SHORT)
                                                .setAction("Action", null).show();
                                        progressBar.dismiss();
                                    }

                                }
                            });

                        }

                    }
        });

        alertDialogBuilder.setNegativeButton("NO",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();




    }


}
