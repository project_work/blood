package com.example.bloodproject;
import android.app.Activity;
import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class custom_adptar extends ArrayAdapter<ShoutHistory> {
    public Activity context;
    public List<ShoutHistory> inlist;


    public custom_adptar(Activity context,List<ShoutHistory> inlist) {
        super(context, R.layout.shoutshow_simple_layout, inlist);
        this.context = context;
        this.inlist = inlist;
    }


    @Override
    public View getView (int position,  View convertView,  ViewGroup parent)   {

        LayoutInflater layoutInflater=context.getLayoutInflater();

        View view=layoutInflater.inflate(R.layout.shoutshow_simple_layout,null,true);
         ShoutHistory insdfert=inlist.get(position);
         TextView needblood=(TextView) view.findViewById(R.id.bloodgrup);
        TextView datea=(TextView) view.findViewById(R.id.datefr);
        TextView name=(TextView) view.findViewById(R.id.names);
        TextView locationa=(TextView) view.findViewById(R.id.locstionfr);
        TextView timea=(TextView) view.findViewById(R.id.times);
        TextView contart=(TextView) view.findViewById(R.id.contactfr);
        TextView donate=(TextView) view.findViewById(R.id.donateblo);
        TextView managedorunmaged=(TextView) view.findViewById(R.id.bloodmangeorunmanaged);


        needblood.setText(""+insdfert.getNeedbl());
        datea.setText(""+insdfert.getDatep());
        name.setText(""+insdfert.getName());
        locationa.setText(""+insdfert.getLoact());

        timea.setText(""+insdfert.getTimep());
        contart.setText(""+insdfert.getPhone());
        donate.setText(""+insdfert.getDonatbl());
        managedorunmaged.setText(""+insdfert.getManaged_or_unmanaged());


        return view;
    }
}
