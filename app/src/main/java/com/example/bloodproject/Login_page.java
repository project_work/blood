package com.example.bloodproject;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.TransitionManager;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.hbb20.CountryCodePicker;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.bloodproject.R.drawable.regestion_succss_button;
import static maes.tech.intentanim.CustomIntent.customType;

public class Login_page extends AppCompatActivity {
    private Toolbar phonelayout,verifilayout,personlayout,bloodlayout,regstarlayout;
    SharedPreferences sp;
    private LinearLayout logolayout,first_layout,second_layout,fourth_Layout,fifth_Layout,sixth_Layout,seventh_Layout;
    //First Layout Button
    private CountryCodePicker codePicker;
    private Button signin, Create_accoount;
    public Uri imageuri;
    private static final int IMAGE_REQUEST=1;
    DatabaseReference reference;
    StorageReference storageReference;
    StorageTask storageTask;
    FirebaseDatabase database;


    //second Layout
    private ImageButton SendVerificationCodeButton;
    private EditText InputUserPhoneNumber;

    //third layout
    private ProgressDialog progressBar;

    //fourth layout

    private EditText InputUserVerificationCode;
    private ImageButton VerifyButton;

    private TextView UserPhoneNumber;

    //fifth layout
    private ImageView uploadimage;
    private Button usernext;
    private EditText userName,emailtx,passwordtx,confirm,address;




    //sixth Layout
    private Button groupA,groupB,gruopAB,groupO,groupAn,groupBn,gruopABn,groupOn,groupContinue;
    String BloodeGroup;
    TextView bloodgroupselect;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks callbacks;
    private FirebaseAuth mAuth;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;

    //seventh Layout
    private TextView UserNameFinal;
    private Button Request;
    MediaPlayer mp,trymp;



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_page);
        mAuth = FirebaseAuth.getInstance();
         database=FirebaseDatabase.getInstance();
         reference=database.getReference("UserProfile");
        storageReference= FirebaseStorage.getInstance().getReference("UserProfile");

        sp = getSharedPreferences("login",MODE_PRIVATE);
        mp=MediaPlayer.create(this,R.raw.restionsuccess);
        trymp=MediaPlayer.create(this,R.raw.tryemail_phone);




        boolean wificonneaction;
        boolean mobileconneaction;
        ConnectivityManager connectivityManager=(ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();
        if (networkInfo !=null && networkInfo.isConnected()){
            wificonneaction=networkInfo.getType()==ConnectivityManager.TYPE_WIFI;
            mobileconneaction=networkInfo.getType()==ConnectivityManager.TYPE_MOBILE;

            if (wificonneaction){

                fullwork();
            }


            else if (mobileconneaction){
                fullwork();


            }



        }else {
            LayoutInflater layoutInflater=LayoutInflater.from(this);
            View view=layoutInflater.inflate(R.layout.no_net_alartdilog,null);
            Button button=view.findViewById(R.id.exit_bt);
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }
            });
            AlertDialog alertDialog=new AlertDialog.Builder(this)
                    .setView(view)
                    .create();
            alertDialog.show();

        }
    }




    private void signInWithPhoneAuthCredential(final PhoneAuthCredential credential)
    {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (credential!=null){
                            if (credential.getSmsCode() != null) {
                                InputUserVerificationCode.setText(credential.getSmsCode());
                                progressBar.dismiss();


                            }
                            else {
                                InputUserVerificationCode.setText("Not Code");
                                InputUserVerificationCode.setTextColor(Color.parseColor("#4bacb8"));
                            }


                        }
                        if (task.isSuccessful())
                        {

                            logolayout.setVisibility(View.GONE);
                            fourth_Layout.setVisibility(View.GONE);
                            verifilayout.setVisibility(View.GONE);
                            fifth_Layout.setVisibility(View.VISIBLE);
                            personlayout.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            /*Toast.makeText(Login_page.this, "Verify Code Success", Toast.LENGTH_SHORT).show();
                            logolayout.setVisibility(View.GONE);
                            fourth_Layout.setVisibility(View.GONE);
                            verifilayout.setVisibility(View.GONE);
                            fifth_Layout.setVisibility(View.VISIBLE);
                            personlayout.setVisibility(View.VISIBLE);*/

                            String message = task.getException().toString();
                            InputUserVerificationCode.setError("Invalid Verify Code");
                            //Toast.makeText(Login_page.this, "Invalid Verify Code "+message, Toast.LENGTH_SHORT).show();
                            progressBar.dismiss();
                        }
                    }
                });
    }

    public  void regestar(){
        String email=emailtx.getText().toString().trim();
        String password=passwordtx.getText().toString().trim();
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {


                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            progressBar.dismiss();
                            Request.setText("Registration is Success");


                            Request.setTextColor(R.color.green);
                            Request.setEnabled(false);
                            FirebaseUser user=mAuth.getCurrentUser();
                            String email=user.getEmail();
                            String Uid=user.getUid();

                            String numberd=InputUserPhoneNumber.getText().toString().trim();
                            String UserName = userName.getText().toString();
                            String addressd=address.getText().toString().trim();
                            String password=passwordtx.getText().toString().trim();

                            HashMap<Object,String>hashMap=new HashMap<>();
                            hashMap.put("email",email);
                            hashMap.put("Phone",numberd);
                            hashMap.put("Name",UserName);
                            hashMap.put("Address",addressd);
                            hashMap.put("Blood",BloodeGroup);
                            hashMap.put("image","");
                            hashMap.put("Uid",Uid);
                            hashMap.put("Password",password);
                            reference.child(Uid).setValue(hashMap);
                            mp.start();

                            sp.edit().putBoolean("logged",true).apply();
                           Intent intent=new Intent(Login_page.this,cardview_home.class);
                            startActivity(intent);


                            //Toast.makeText(getApplication(),"Registration is Success",Toast.LENGTH_LONG).show();

                        }




                        else  {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                trymp.start();
                                progressBar.dismiss();
                                Request.setText("Already Registration");
                                Request.setTextColor(R.color.red);

                                //Toast.makeText(getApplication(),"Already Registration",Toast.LENGTH_LONG).show();

                            }
                            else {

                                Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();
                                progressBar.dismiss();
                            }


                        }

                    }
                });


    }

    public void backwelcomepage(View view) {

        phonelayout.setVisibility(View.GONE);
        second_layout.setVisibility(View.GONE);
        first_layout.setVisibility(View.VISIBLE);
        logolayout.setVisibility(View.VISIBLE);
    }

    public void backphoneLayout(View view) {
        verifilayout.setVisibility(View.GONE);
        fourth_Layout.setVisibility(View.GONE);
        phonelayout.setVisibility(View.VISIBLE);
        second_layout.setVisibility(View.VISIBLE);
        logolayout.setVisibility(View.VISIBLE);
    }

    public void backverifiLayout(View view) {

        fifth_Layout.setVisibility(View.GONE);
        personlayout.setVisibility(View.GONE);
        verifilayout.setVisibility(View.VISIBLE);
        fourth_Layout.setVisibility(View.VISIBLE);
        logolayout.setVisibility(View.VISIBLE);
    }

    public void backpersonLayout(View view) {
        sixth_Layout.setVisibility(View.GONE);
        bloodlayout.setVisibility(View.GONE);
        fifth_Layout.setVisibility(View.VISIBLE);
        personlayout.setVisibility(View.VISIBLE);



    }

    public void backbloodLayout(View view) {
        seventh_Layout.setVisibility(View.GONE);
        regstarlayout.setVisibility(View.GONE);
        sixth_Layout.setVisibility(View.VISIBLE);
        bloodlayout.setVisibility(View.VISIBLE);

    }


@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public void fullwork(){
    //login page keep
    if (
            sp.getBoolean("logged",false))

    {
        startActivity(new Intent(Login_page.this,cardview_home.class));
        customType(Login_page.this,"up-to-bottom");

        finish();
    }

    mAuth = FirebaseAuth.getInstance();

    codePicker=(CountryCodePicker)findViewById(R.id.ccp);
    bloodgroupselect=(TextView)findViewById(R.id.bloodgrouselectid);

    emailtx=(EditText)findViewById(R.id.email);
    passwordtx=(EditText)findViewById(R.id.password);
    address=(EditText)findViewById(R.id.address);
    confirm=(EditText)findViewById(R.id.confirampassword);
    uploadimage=(ImageView)findViewById(R.id.regUserPhoto);

    phonelayout = (Toolbar) findViewById(R.id.createaccountToolbar);
    verifilayout = (Toolbar) findViewById(R.id.phoneveriToolbar);
    personlayout = (Toolbar) findViewById(R.id.personinfoToolbar);
    bloodlayout = (Toolbar) findViewById(R.id.bloodToolbar);
    regstarlayout = (Toolbar) findViewById(R.id.registorToolbar);


    logolayout = (LinearLayout) findViewById(R.id.logolayout);
    first_layout = (LinearLayout) findViewById(R.id.first_layout);
    second_layout = (LinearLayout) findViewById(R.id.second_layout);
    fourth_Layout = (LinearLayout) findViewById(R.id.fourth_layout);
    fifth_Layout = (LinearLayout) findViewById(R.id.five_layout);
    sixth_Layout = (LinearLayout) findViewById(R.id.sixth_layout);
    seventh_Layout = (LinearLayout) findViewById(R.id.seventh_layout);
    progressBar = new ProgressDialog(this);


    logolayout.setVisibility(View.VISIBLE);
    first_layout.setVisibility(View.VISIBLE);
    final ViewGroup transitionsContainerfirst = (ViewGroup) findViewById(R.id.first_layout);
    TransitionManager.beginDelayedTransition(transitionsContainerfirst);

    signin = (Button) transitionsContainerfirst.findViewById(R.id.signin);
    Create_accoount = (Button) transitionsContainerfirst.findViewById(R.id.create_account);

    uploadimage.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    });


    signin.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(Login_page.this,login_page2.class));
            customType(Login_page.this,"left-to-right");

            finish();

        }
    });

    Create_accoount.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            first_layout.setVisibility(View.GONE);
            second_layout.setVisibility(View.VISIBLE);
            phonelayout.setVisibility(View.VISIBLE);


        }
    });
    final ViewGroup transitionsContainersecond = (ViewGroup) findViewById(R.id.second_layout);
    TransitionManager.beginDelayedTransition(transitionsContainersecond);



    InputUserPhoneNumber = (EditText) transitionsContainersecond.findViewById(R.id.edittext_phone);
    SendVerificationCodeButton = (ImageButton) transitionsContainersecond.findViewById(R.id.input_phoneNumber);


    SendVerificationCodeButton.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {

            String phoneNumber = InputUserPhoneNumber.getText().toString();
            String country=codePicker.getSelectedCountryCode();
            String nu=("+"+country+""+phoneNumber);
            //off
                        /*phonelayout.setVisibility(View.GONE);
                        second_layout.setVisibility(View.GONE);
                        fourth_Layout.setVisibility(View.VISIBLE);
                        verifilayout.setVisibility(View.VISIBLE);*/
            //off
            //next Opeen
            if (phoneNumber.isEmpty()){
                InputUserPhoneNumber.setError("Please enter your phone number first...");
                InputUserPhoneNumber.requestFocus();
            }


            if (TextUtils.isEmpty(phoneNumber))
            {
                Toast.makeText(Login_page.this, "Please enter your phone number first...", Toast.LENGTH_SHORT).show();
            }




            else
            {
                progressBar.setTitle("Phone Verification");
                progressBar.setMessage("Please wait, while we are authenticating using your phone...");
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();
                PhoneAuthProvider.getInstance().verifyPhoneNumber(nu, 60, TimeUnit.SECONDS, Login_page.this, callbacks);
                phonelayout.setVisibility(View.GONE);
                second_layout.setVisibility(View.GONE);
                fourth_Layout.setVisibility(View.VISIBLE);
                verifilayout.setVisibility(View.VISIBLE);

            }
            UserPhoneNumber.setText(nu);

        }
    });
    final ViewGroup transitionsContainerforth = (ViewGroup) findViewById(R.id.fourth_layout);
    TransitionManager.beginDelayedTransition(transitionsContainerfirst);

    InputUserVerificationCode = (EditText) transitionsContainerforth.findViewById(R.id.verifiednumber);
    VerifyButton = (ImageButton) transitionsContainerforth.findViewById(R.id.verified_next);
    UserPhoneNumber = (TextView) transitionsContainerforth.findViewById(R.id.userPhoneNumber);



    VerifyButton.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            //off
                        /*logolayout.setVisibility(View.GONE);
                        fourth_Layout.setVisibility(View.GONE);
                        verifilayout.setVisibility(View.GONE);
                        fifth_Layout.setVisibility(View.VISIBLE);
                        personlayout.setVisibility(View.VISIBLE);*/
            //off





            String verificationCode = InputUserVerificationCode.getText().toString();

            if (verificationCode.isEmpty())
            {
                InputUserVerificationCode.setError("Please write verification code");
                InputUserVerificationCode.requestFocus();
                //Toast.makeText(Login_page.this, "Please write verification code first...", Toast.LENGTH_SHORT).show();
            }
            else
            {
                progressBar.setTitle("Verification Code");
                progressBar.setMessage("Please wait, while we are verifying verification code...");
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, verificationCode);
                signInWithPhoneAuthCredential(credential);



            }

        }
    });
    callbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential)
        {
            signInWithPhoneAuthCredential(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e)
        {
            Toast.makeText(Login_page.this, "Invalid Phone Number, Please enter correct phone number with your country code..."+e, Toast.LENGTH_LONG).show();
            progressBar.dismiss();


        }

        public void onCodeSent(String verificationId,
                               PhoneAuthProvider.ForceResendingToken token)
        {
            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
            mResendToken = token;


            Toast.makeText(Login_page.this, "Code has been sent, please check and verify...", Toast.LENGTH_SHORT).show();
            progressBar.dismiss();


        }
    };



    final ViewGroup transitionsContainerfifth = (ViewGroup) findViewById(R.id.five_layout);
    TransitionManager.beginDelayedTransition(transitionsContainerfifth);

    userName = (EditText) transitionsContainerfifth.findViewById(R.id.userName);
    usernext = (Button) transitionsContainerfifth.findViewById(R.id.usernamenext);
    String UserName = userName.getText().toString();
    usernext.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            String UserName = userName.getText().toString();
            String email=emailtx.getText().toString().trim();
            String password=passwordtx.getText().toString().trim();
            String addressd=address.getText().toString().trim();
            String confirmg=confirm.getText().toString().trim();

                        /*if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                            emailtx.setError("Email Not Valid");
                            emailtx.requestFocus();
                            return;
                        }

                        if (password.isEmpty()){

                            passwordtx.setError("Password Enter");
                            passwordtx.requestFocus();
                            return;
                        }
                        if (password.length()<6){
                            passwordtx.setError("Minimum Password 6 ");
                            passwordtx.requestFocus();
                            return;

                        }
                        if (!password.equals(confirmg)){

                            passwordtx.setError("Password Not Match ");
                            passwordtx.requestFocus();
                        }
                        if (UserName.isEmpty()){

                            userName.setError("User Name Enter");
                            userName.requestFocus();
                            return;
                        }
                        if (addressd.isEmpty()){

                            address.setError("Address Enter");
                            address.requestFocus();
                            return;
                        }

                        else*/
            {
                fifth_Layout.setVisibility(View.GONE);
                personlayout.setVisibility(View.GONE);
                sixth_Layout.setVisibility(View.VISIBLE);
                bloodlayout.setVisibility(View.VISIBLE);
            }
        }
    });








    groupA = (Button) findViewById(R.id.groupA);
    groupB = (Button) findViewById(R.id.groupB);
    gruopAB = (Button) findViewById(R.id.groupAB);
    groupO = (Button) findViewById(R.id.groupO);
    groupAn=(Button)findViewById(R.id.groupA_);
    groupBn=(Button)findViewById(R.id.groupB_);
    gruopABn=(Button)findViewById(R.id.groupAB_);
    groupOn=(Button)findViewById(R.id.groupO_);
    groupContinue = (Button) findViewById(R.id.groupContinue);

    groupA.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupA.setBackground(getDrawable(R.drawable.create_account_background));
            groupA.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "A+";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);

        }
    });
    groupB.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupB.setBackground(getDrawable(R.drawable.create_account_background));
            groupB.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "B+";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });

    gruopAB.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            gruopAB.setBackground(getDrawable(R.drawable.create_account_background));
            gruopAB.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "AB+";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });

    groupO.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupO.setBackground(getDrawable(R.drawable.create_account_background));
            groupO.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "O+";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });

    groupAn.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupAn.setBackground(getDrawable(R.drawable.create_account_background));
            groupAn.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "A-";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });
    groupBn.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupBn.setBackground(getDrawable(R.drawable.create_account_background));
            groupBn.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "B-";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });
    gruopABn.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            gruopABn.setBackground(getDrawable(R.drawable.create_account_background));
            gruopABn.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "AB-";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });
    groupOn.setOnClickListener(new OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onClick(View v) {
            groupOn.setBackground(getDrawable(R.drawable.create_account_background));
            groupOn.setTextColor(getResources().getColor(R.color.white));
            BloodeGroup = "O-";
            bloodgroupselect.setText("You Select BloodGroup :" +BloodeGroup);
        }
    });


    groupContinue.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
            sixth_Layout.setVisibility(View.GONE);
            bloodlayout.setVisibility(View.GONE);
            regstarlayout.setVisibility(View.VISIBLE);
            seventh_Layout.setVisibility(View.VISIBLE);
            String UserName = userName.getText().toString();
            UserNameFinal.setText(UserName);
        }
    });

    UserNameFinal = (TextView) findViewById(R.id.userNameFinal);
    Request = (Button) findViewById(R.id.request);


    Request.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {



            progressBar.setTitle("Registration");
            progressBar.setMessage("Please wait, while we are Account Create");
            progressBar.setIcon(R.drawable.eegistration24px);
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();

            regestar();
        }


    });
}

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        android.app.AlertDialog alertbox = new android.app.AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }

    }



