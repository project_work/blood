package com.example.bloodproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;

import static maes.tech.intentanim.CustomIntent.customType;

public class blood_day_opokareta extends AppCompatActivity {
    public ListView listView;
    String[] tipsarry;
    public int[] govimage={R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,
            R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips,R.drawable.bloodicontips
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_day_opokareta);
        listView=(ListView)findViewById(R.id.opokartetas);
        tipsarry =getResources().getStringArray(R.array.blooddayopokareta);
        tipslist_adapter cu=new tipslist_adapter(this, govimage,tipsarry);
        listView.setAdapter(cu);
    }

    public void cardpageback(View view) {
        startActivity(new Intent(blood_day_opokareta.this,ke_kake_donate_blood.class));
        customType(blood_day_opokareta.this,"right-to-left");
        finish();
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(blood_day_opokareta.this,ke_kake_donate_blood.class));
            customType(blood_day_opokareta.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
