package com.example.bloodproject;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;

import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;

import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import static maes.tech.intentanim.CustomIntent.customType;

public class shoutPage_sendsms extends AppCompatActivity  implements DatePickerDialog.OnDateSetListener   {
    private static final int REQUEST_LOCATION = 1;
    final int SEND_SMS_PERMISSION_REQUEST_CODE = 1;
    private Spinner needblood;
   CheckBox app,bpp,opp,abpp,ann,bnn,onn,abnn;
    Button datepic,timepic;
    EditText locatined,describe;
    Button sendbtn;
    StringBuffer   firebasequeryphone=new StringBuffer();
    private ProgressDialog progressBar;

    String  Name,Phone;
    public List<ShoutHistory> inlist;
    public custom_adptar ff;

   TimePickerDialog timePickerDialog;
    LocationManager locationManager;
    String lattitude,longitude;
    Geocoder geocoder;
    List<Address> addressList;
    FirebaseUser user;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    DatabaseReference dateref;
    FirebaseDatabase fireba;
    FirebaseAuth fut ;
    String area;
    MediaPlayer mp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shout_page_sendsms);
        dateref= FirebaseDatabase.getInstance().getReference("ShoutHistory");



        needblood=(Spinner)findViewById(R.id.needbloodSpreen);

        datepic=(Button)findViewById(R.id.datepick);
        timepic=(Button) findViewById(R.id.timepic);
        locatined=(EditText)findViewById(R.id.locationedit);
        describe=(EditText) findViewById(R.id.describe);
        sendbtn=(Button)findViewById(R.id.sendsmsbtn);

        app=(CheckBox)findViewById(R.id.ap);
        bpp=(CheckBox)findViewById(R.id.bp);
        opp=(CheckBox)findViewById(R.id.op);
        abpp=(CheckBox)findViewById(R.id.abp);
        ann=(CheckBox)findViewById(R.id.an);
        bnn=(CheckBox)findViewById(R.id.bn);
        onn=(CheckBox)findViewById(R.id.on);
        abnn=(CheckBox)findViewById(R.id.abn);
        mp=MediaPlayer.create(this,R.raw.postmp);
        mp.start();

        progressBar = new ProgressDialog(this);
        inlist =new ArrayList<>();
        ff  =new custom_adptar(shoutPage_sendsms.this,inlist);
        sendbtn.setEnabled(false);
        if(checkPermission(Manifest.permission.SEND_SMS)){
            sendbtn.setEnabled(true);
        }else{
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS}, SEND_SMS_PERMISSION_REQUEST_CODE);
        }


        imagebloodshow();





        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
        else if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
        }



        List<String> catagory=new ArrayList<>();
        catagory.add(0,"");
        catagory.add("A+");
        catagory.add("B+");
        catagory.add("AB+");
        catagory.add("A-");
        catagory.add("B-");
        catagory.add("Ab-");
        catagory.add("O+");
        catagory.add("O-");
        List<String> donating=new ArrayList<>();
        donating.add(0,"Donate Blood Group");
        donating.add("A+");
        donating.add("B+");
        donating.add("AB+");
        donating.add("A-");
        donating.add("B-");
        donating.add("Ab-");
        donating.add("O+");
        donating.add("O-");

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.simpel_speenar_layout,R.id.texview_sampel_speenar,catagory);
        needblood.setAdapter(adapter);


        sendbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String datepc= datepic.getText().toString();
                String timepc= timepic.getText().toString();
                String number= describe.getText().toString();
                String location= locatined.getText().toString();


                String donatbl="";
                if (app.isChecked()){

                    donatbl +="A+,";
                }
                if (bpp.isChecked()){

                    donatbl +="B+,";
                }
                if (opp.isChecked()){

                    donatbl +="O+,";
                }
                if (abpp.isChecked()){

                    donatbl +="AB+,";
                }
                if (ann.isChecked()){

                    donatbl +="A-,";
                }
                if (bnn.isChecked()){

                    donatbl +="B-,";
                }
                if (onn.isChecked()){

                    donatbl +="O-,";
                }
                if (abnn.isChecked()){

                    donatbl +="AB-,";
                }

             else  if (needblood.getSelectedItemPosition()==0){

                    Toast.makeText(getApplication(),"Select Need Blood Group",Toast.LENGTH_LONG).show();
                }

                else if (donatbl==null){
                    Toast.makeText(getApplication(),"Select Donate Blood Group",Toast.LENGTH_LONG).show();


                }
                else  if (datepc.isEmpty()){
                    Toast.makeText(getApplication(),"Select Date",Toast.LENGTH_LONG).show();


                }
                else  if (timepc.isEmpty()){
                    Toast.makeText(getApplication(),"Select Time",Toast.LENGTH_LONG).show();


                }
                else  if (number.isEmpty()){
                    Toast.makeText(getApplication(),"Give your Number",Toast.LENGTH_LONG).show();


                }
                else  if (location.isEmpty()){
                    Toast.makeText(getApplication(),"Give your Loacation",Toast.LENGTH_LONG).show();


                }



               else {
                   progressBar.setTitle("Send");
                   progressBar.setMessage("Please wait,Send Your Sms");
                   progressBar.setIcon(R.drawable.ic_menu_send);
                   progressBar.setCanceledOnTouchOutside(false);
                   progressBar.show();
                    numberquery();
                    smsSend();
                    shoutsmsdatebasestor();

               }
            }
        });


        datepic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getLocation();
                dateshow();
                numberquery();

            }
        });
        timepic.setOnClickListener(new View.OnClickListener() {
            java.util.Calendar calendar= java.util.Calendar.getInstance();
            int hour=calendar.get(Calendar.HOUR);
            int mint=calendar.get(Calendar.MINUTE);

            @Override
            public void onClick(View v) {
                timePickerDialog=new TimePickerDialog(shoutPage_sendsms.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String am_pm;
                        if (hourOfDay<12){
                            am_pm="AM";
                            timepic.setText(hourOfDay+":"+minute+" "+am_pm);
                        }
                        else if (hourOfDay==12){
                            am_pm="PM";
                            timepic.setText(hourOfDay+":"+minute+" "+am_pm);

                        }
                        else {
                            am_pm="PM";
                            timepic.setText(hourOfDay+":"+minute+" "+am_pm);
                        }


                    }
                },hour,mint,true);

                timePickerDialog.show();
            }
        });




    }




    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        String daty= +dayOfMonth+"/"+(month+1)+"/"+year;
        datepic.setText(daty);

    }

    public void dateshow(){

        DatePickerDialog datePickerDialog=new DatePickerDialog(
                this,this,
                java.util.Calendar.getInstance().get(Calendar.YEAR),
                java.util.Calendar.getInstance().get(Calendar.MONTH),
                java.util.Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        );
        datePickerDialog.show();
    }



    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(shoutPage_sendsms.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (shoutPage_sendsms.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(shoutPage_sendsms.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        } else {
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            Location location2 = locationManager.getLastKnownLocation(LocationManager. PASSIVE_PROVIDER);

            if (location != null) {
                double latti = location.getLatitude();
                double longi = location.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                     area=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    locatined.setText(address);

                } catch (IOException e) {
                    e.printStackTrace();
                }




            } else  if (location1 != null) {
                double latti = location1.getLatitude();
                double longi = location1.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                    String are=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    locatined.setText(address);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else  if (location2 != null) {
                double latti = location2.getLatitude();
                double longi = location2.getLongitude();
                lattitude = String.valueOf(latti);
                longitude = String.valueOf(longi);
                geocoder=new Geocoder(this, Locale.getDefault());
                try {
                    addressList=geocoder.getFromLocation(latti,longi,1);
                    String address=addressList.get(0).getAddressLine(0);
                    area=addressList.get(0).getLocality();
                    String city=addressList.get(0).getAdminArea();
                    String county=addressList.get(0).getCountryName();

                    locatined.setText(""+address);

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }else{

                Toast.makeText(this,"Unble to Trace your location",Toast.LENGTH_SHORT).show();

            }
        }
    }
    protected void buildAlertMessageNoGps() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please Turn ON your GPS Connection")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public void imagebloodshow(){

        firebaseAuth= FirebaseAuth.getInstance();
        user=firebaseAuth.getCurrentUser();
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference("UserProfile");
        Query query=databaseReference.orderByChild("email").equalTo(user.getEmail());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds:dataSnapshot.getChildren()){
                     Name=""+ds.child("Name").getValue();
                     Phone=""+ds.child("Phone").getValue();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public void smsSend(){
        String loact=locatined.getText().toString();
        String needbl=needblood.getSelectedItem().toString();
        String donatbl="";
        if (app.isChecked()){

            donatbl +="A+,";
        }
        if (bpp.isChecked()){

            donatbl +="B+,";
        }
        if (opp.isChecked()){

            donatbl +="O+,";
        }
        if (abpp.isChecked()){

            donatbl +="AB+,";
        }
        if (ann.isChecked()){

            donatbl +="A-,";
        }
        if (bnn.isChecked()){

            donatbl +="B-,";
        }
        if (onn.isChecked()){

            donatbl +="O-,";
        }
        if (abnn.isChecked()){

            donatbl +="AB-,";
        }
        String phone=describe.getText().toString();
        String phoneNo="null";
        phoneNo=firebasequeryphone.toString();


        //String smsf="Hello dear";

        String smsf="Hi, I am "+Name+". I need "+needbl+" Blood.And I am ready to donate "+donatbl+" Blood.My Location Is " +loact+". You Can contact me at "+phone+".";



        try {String number[]=phoneNo.split(" ");
            if(checkPermission(Manifest.permission.SEND_SMS)){
            SmsManager smsManager = SmsManager.getDefault();

            for( String nu:number) {
                smsManager.sendTextMessage(nu, null, smsf, null, null);
                Toast.makeText(getApplicationContext(), "SMS Sent!",
                        Toast.LENGTH_LONG).show();

            }
            }
            else{
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }


        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(),""+smsf,Toast.LENGTH_LONG).show();
            Toast.makeText(getApplicationContext(),
                    "SMS faild,"+e,
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public void shoutsmsdatebasestor(){

        String donatbl="";
        if (app.isChecked()){

            donatbl +="A+";
        }
        if (bpp.isChecked()){

            donatbl +="B+";
        }
        if (opp.isChecked()){

            donatbl +="O+";
        }
        if (abpp.isChecked()){

            donatbl +="AB+";
        }
        if (ann.isChecked()){

            donatbl +="A-";
        }
        if (bnn.isChecked()){

            donatbl +="B-";
        }
        if (onn.isChecked()){

            donatbl +="O-";
        }
        if (abnn.isChecked()){

            donatbl +="AB-";
        }

        FirebaseUser user=firebaseAuth.getCurrentUser();
        String email=user.getEmail();
        String Uid=user.getUid();
        String loact=locatined.getText().toString();
        String needbl=needblood.getSelectedItem().toString();
        //String donatbl=donatblood.getSelectedItem().toString();
        String decrife=describe.getText().toString();
        String datep=datepic.getText().toString();
        String timep=timepic.getText().toString();
        //HashMap<Object,String> hashMap=new HashMap<>();
        /*
        hashMap.put("Emai",email);
        hashMap.put("NeedBlood",needbl);
        hashMap.put("DonateBlood",donatbl);
        hashMap.put("Location",loact);
        hashMap.put("TakeIt",decrife);
        hashMap.put("Date",datep);
        hashMap.put("Time",timep);
        */
        //hashMap.put("Uid2",Uid);
        String smsUId=dateref.push().getKey();
        String managed_or_unmanaged="Unmanaged";
        ShoutHistory as=new ShoutHistory(area, managed_or_unmanaged,smsUId,email,Uid,loact,needbl,donatbl,decrife,datep,timep,Name,decrife);

        //databaseReference.child(Uid).setValue(hashMap);

        //databaseReference.child(da).setValue(hashMap);
        dateref.child(smsUId).setValue(as);
        progressBar.dismiss();
        Toast.makeText(getApplication(),"Successfully Save",Toast.LENGTH_LONG).show();


    }

    public void backhomepg(View view) {
        startActivity(new Intent(shoutPage_sendsms.this,cardview_home.class));
        customType(shoutPage_sendsms.this,"bottom-to-up");

        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(shoutPage_sendsms.this,cardview_home.class));
            customType(shoutPage_sendsms.this,"bottom-to-up");

            finish();
            //exitByBackKey();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


public void numberquery(){
    final String needbl=needblood.getSelectedItem().toString();

    String donatbl="";
    if (app.isChecked()){

        donatbl +="A+";
    }
    if (bpp.isChecked()){

        donatbl +="B+";
    }
    if (opp.isChecked()){

        donatbl +="O+";
    }
    if (abpp.isChecked()){

        donatbl +="AB+";
    }
    if (ann.isChecked()){

        donatbl +="A-";
    }
    if (bnn.isChecked()){

        donatbl +="B-";
    }
    if (onn.isChecked()){

        donatbl +="O-";
    }
    if (abnn.isChecked()){

        donatbl +="AB-";
    }
     firebaseAuth= FirebaseAuth.getInstance();
    user=firebaseAuth.getCurrentUser();
    firebaseDatabase=FirebaseDatabase.getInstance();
    //databaseReference=firebaseDatabase.getReference("ShoutHistory");
    Query query=dateref.orderByChild("area").equalTo(area);
    final String finalDonatbl = donatbl;
    query.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (DataSnapshot ds:dataSnapshot.getChildren()){

                ShoutHistory inserty=ds.getValue(ShoutHistory.class);
                inlist.add(inserty);
                if (inserty.getNeedbl().equals(finalDonatbl) && inserty.getDonatbl().equals(needbl) ){
                    inlist.add(inserty);
                    firebasequeryphone.append(", "+inserty.getPhone());
                    Toast.makeText(getApplication(), "error" +firebasequeryphone, Toast.LENGTH_LONG).show();

                }


            }

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Toast.makeText(getApplication(),"error"+databaseError.getMessage(),Toast.LENGTH_LONG).show();

        }
    });

}

    public boolean checkPermission(String permission){
        int check = ContextCompat.checkSelfPermission(this, permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

}
