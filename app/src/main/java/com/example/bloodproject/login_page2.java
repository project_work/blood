package com.example.bloodproject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.util.TimeUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static maes.tech.intentanim.CustomIntent.customType;

public class login_page2 extends AppCompatActivity {

    SharedPreferences sp;
    //layout login
    LinearLayout loginlayout;
    EditText editText1,editText2;
    Button button;
    private ProgressDialog progressBar;
    private FirebaseAuth firebaseAuth;
    FirebaseUser user;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    //layout forgot password
    LinearLayout forgotlayout;
    EditText editTextforgot;
    Button buttonforgotpassword;
    TextView textView;
    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page2);
        setTitle("Login");

        sp = getSharedPreferences("login",MODE_PRIVATE);
        databaseReference= FirebaseDatabase.getInstance().getReference("bloodproject-567cc");
        firebaseAuth = FirebaseAuth.getInstance();
        loginlayout=(LinearLayout)findViewById(R.id.layoutlogin);
        forgotlayout=(LinearLayout)findViewById(R.id.layoutforgot);
        textView=(TextView)findViewById(R.id.passwordsendconfor);
        editTextforgot=(EditText)findViewById(R.id.forgotemil);
        buttonforgotpassword=(Button)findViewById(R.id.forgotbutton);

        editText1=(EditText)findViewById(R.id.emaillog);
        editText2=(EditText)findViewById(R.id.passwordlog);
        button=(Button)findViewById(R.id.loginbtn);
        progressBar = new ProgressDialog(this);
         mp=MediaPlayer.create(this,R.raw.welcometone);
        FirebaseApp.initializeApp(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email=editText1.getText().toString().trim();
                String password=editText2.getText().toString().trim();
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    editText1.setError("Email Not Valid");
                    editText1.requestFocus();
                    return;
                }

                if (password.isEmpty()){

                    editText2.setError("Password Null");
                    editText2.requestFocus();
                    return;
                }
                if (password.length()<6){
                    editText2.setError("Minimum Password 6 ");
                    editText2.requestFocus();
                    return;

                }
                progressBar.setTitle("Login");
                progressBar.setMessage("Please wait,We are Account Login");
                progressBar.setIcon(R.drawable.lodingicon);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();

                login();



            }
        });
        buttonforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=  editTextforgot.getText().toString().trim();
                if (email.isEmpty()){

                    editTextforgot.setError("Email Write");
                    editTextforgot.requestFocus();
                    return;
                }
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

                    editTextforgot.setError("Email Not Valid");
                    editTextforgot.requestFocus();
                    return;
                }

                progressBar.setTitle("Forgot Password");
                progressBar.setMessage("Please wait,We are sending your Password email");
                progressBar.setIcon(R.drawable.passwordforgot);
                progressBar.setCanceledOnTouchOutside(false);
                progressBar.show();
                forgotpass();
            }
        });
    }



    public void login(){
        String email=editText1.getText().toString().trim();
        String password=editText2.getText().toString().trim();

        /*if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){

            editText1.setError("Email Not Valid");
            editText1.requestFocus();
            return;
        }*/

        if (password.isEmpty()){

            editText2.setError("Password Null");
            editText2.requestFocus();
            return;
        }
        if (password.length()<6){
            editText2.setError("Minimum Password 6 ");
            editText2.requestFocus();
            return;

        }







        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new
                OnCompleteListener<AuthResult>() {


                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            progressBar.dismiss();
                            mp.start();

                            sp.edit().putBoolean("logged",true).apply();
                            startActivity(new Intent(login_page2.this,cardview_home.class));
                            customType(login_page2.this,"up-to-bottom");
                            finish();
                            FirebaseUser user=firebaseAuth.getCurrentUser();

                        }

                        else {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException){
                                progressBar.dismiss();
                                button.setText("Already Login");
                                button.setTextColor(R.color.green);
                                //Toast.makeText(getApplication(),"",Toast.LENGTH_LONG).show();

                            }
                            else {
                                progressBar.dismiss();
                                button.setTextColor(R.color.red);
                                Toast.makeText(getApplication(),"Error :"+ task.getException().getMessage(),Toast.LENGTH_LONG).show();

                            }


                        }
                    }
                });





    }
     public void forgotpass(){
         String email=  editTextforgot.getText().toString().trim();
        firebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
              if (task.isSuccessful()){
                  progressBar.dismiss();
                  textView.setText("Please Chack Your Email Address");
                  //Toast.makeText(login_page2.this, "Password Send To Your Email ", Toast.LENGTH_LONG).show();

              }
              else {
                  progressBar.dismiss();
                  Toast.makeText(login_page2.this, "Error "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
              }
            }
        });

    }

    public void loginlayoutopen(View view) {
        loginlayout.setVisibility(View.GONE);
        forgotlayout.setVisibility(View.VISIBLE);
    }

    public void backloginLayout(View view) {
        loginlayout.setVisibility(View.VISIBLE);
        forgotlayout.setVisibility(View.GONE);
    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //exitByBackKey();
            startActivity(new Intent(login_page2.this,Login_page.class));
            customType(login_page2.this,"right-to-left");

            finish();

            //moveTaskToBack(false);

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void exitByBackKey() {

        AlertDialog alertbox = new AlertDialog.Builder(this)
                .setMessage("Do you want to exit application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {

                        finish();
                        //close();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {

                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                })
                .show();

    }


    public void createAccount(View view) {
        Intent intent=new Intent(login_page2.this,Login_page.class);
        startActivity(intent);
        finish();
    }
}
